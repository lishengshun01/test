﻿
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
//using Photon;
public class AllSceneName
{
    //public const string TheGraveyard = "TheGraveyard";
    public const string Test = "Test";
    //public const string StageView = "StageView";
    //public const string StageMenu = "StageMenu";
}

public class SceneController
{
    public static string CurrentScene = AllSceneName.Test;
    public static List<string> ListCurrentSubScene = new List<string>();

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void OnBeforeSceneLoadRuntimeMethod()
    {
        DataController.LoadData();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }


    //	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    //	static void OnAfterSceneLoadRuntimeMethod()
    //	{
    //		
    //	}
    public static void OpenScene(string _SceneName)
    {
        if (CurrentScene != _SceneName)
        {
            CurrentScene = _SceneName;
            ListCurrentSubScene.Clear();
            SceneManager.LoadScene(_SceneName, LoadSceneMode.Single);
        }

    }
    static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //CreateController.Instance.Canvas = GameObject.FindObjectOfType<Canvas>().transform;
    }

    public static void OpenSubScene(string _SceneName)
    {
        if (!ListCurrentSubScene.Contains(_SceneName))
        {
            ListCurrentSubScene.Add(_SceneName);
            SceneManager.LoadScene(_SceneName, LoadSceneMode.Additive);
        }
    }

    //	public static void OpenSubScenePhoton(string _SceneName)
    //	{
    //		if (!ListCurrentSubScene.Contains(_SceneName)) {
    //			if (PhotonNetwork.CurrentRoom.PlayerCount == 1) {
    //				ListCurrentSubScene.Add (_SceneName);
    //				PhotonNetwork.LoadLevel (_SceneName, LoadSceneMode.Additive);
    //			}
    //		}
    //	}

    public static void CloseSubScene(string _SceneName)
    {
        if (ListCurrentSubScene.Contains(_SceneName))
        {
            ListCurrentSubScene.Remove(_SceneName);
            SceneManager.UnloadSceneAsync(_SceneName);
        }
    }

}
