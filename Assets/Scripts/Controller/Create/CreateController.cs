﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class CreateController : SingletonMonoBehavior<CreateController>
{
    public GameObject[] obstacles;
    public GameObject AdditionalItem;
    public GameObject DesiredItem;
    //public PlayerController prefabPlayer;

    public GameObject createObstacle(Vector3 spawnPos,Quaternion rotation,int id)
    {
        GameObject obstacle = GameObject.Instantiate(obstacles[id], Vector3.zero, rotation);
        obstacle.transform.localPosition = spawnPos;
        return obstacle;
    }

    public GameObject createItem(Vector3 spawnPos, Quaternion rotation)
    {
        GameObject additionalItem = GameObject.Instantiate(AdditionalItem, Vector3.zero, rotation);
        additionalItem.transform.localPosition = spawnPos;
        return additionalItem;
    }

    public GameObject createDesiredItem(Vector3 spawnPos, Quaternion rotation)
    {
        GameObject desiredItem = GameObject.Instantiate(DesiredItem, Vector3.zero, rotation);
        desiredItem.transform.localPosition = spawnPos;
        return desiredItem;
    }

    //public PlayerController SpawnPlayer(Vector3 spawnPos,Quaternion rotation)
    //{
    //    PlayerController playerController = GameObject.Instantiate(prefabPlayer, Vector3.zero, rotation) as PlayerController;
    //    playerController.transform.localPosition = spawnPos;
    //    return playerController;
    //}

}