﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : Obstacle1
{
    public override void Start()
    {
        base.Start();
        setDelta(3.3f);
    }
    public override void hitBehavior()
    {
        //ManagerController.Instance.player.GetComponent<PlayerController>().moveForward();
        ManagerController.Instance.OnPerfect();
        player.canAttack = true;
        //SpaceObserver.Instance.Notify(TOPICNAME.Perfect);
        Destroy(gameObject);
    }
}
