﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TOPICNAME
{
    public const string WIN = "Win";
    public const string LOSE = "Lose";
    public const string Perfect = "Perfect";
}
public enum GameState
{
    IsPlaying,
    IsOver,
    None
}

public class ManagerController : Singleton<ManagerController>
{

    public static int Life = 3;
    public GameObject player;
    //public Text perfect;
    public UIController ui;
    //public Camera cam;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnWin()
    {
        //Debug.Log("won");
        ui.darkScreen.SetActive(true);
        ui.noteText.text = "Won";
        ui.noteText.color = Color.cyan;
        //ui.noteText.GetComponent<TextPopupController>().startHide();
        //perfect.gameObject.SetActive(true);
        ui.noteText.enabled = true;
        //onReplay();
    }

    public void OnLose()
    {
        //Debug.Log("won");
        ui.darkScreen.SetActive(true);
        ui.noteText.text = "Lose";
        ui.noteText.color = Color.white;
        //ui.noteText.GetComponent<TextPopupController>().startHide();
        //perfect.gameObject.SetActive(true);
        ui.noteText.enabled = true;
        //onReplay();
    }

    public void OnPerfect()
    {
        //Debug.Log("a");
        ui.noteText.text = "Perfect";
        //perfect.gameObject.SetActive(true);
        ui.noteText.GetComponent<TextPopupController>().startHide();
        ui.noteText.enabled = true;
    }

    public void onTargetHit()
    {
        ui.OL.text = (int.Parse(ui.OL.text)-1).ToString();
    }

    public void onReplay()
    {
        //Debug.Log("A");
        ui.OL.text = StageController.Instance.stageInfo.obstaclePosX.Length.ToString();
        ui.Ammo.text = "3";
        ui.darkScreen.SetActive(false);
        player.GetComponent<PlayerController>().onUpdatePlayer();
        StageController.Instance.onReplay();
    }

    //void OnDestroy()
    //{

    //}
}
