﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle1 : ObstacleController //left right
{
    float delta = 3f;  // Amount to move left and right from the start point
    public float speed = 2f;
    private Vector3 startPos;

    public override void Start()
    {
        base.Start();
        startPos = new Vector3(33,transform.position.y,transform.position.z);
    }

    void Update()
    {
        Vector3 v = startPos;
        v.x += delta * Mathf.Sin(Time.time * speed);
        transform.position = v;
    }

    public void setDelta(float newDelta)
    {
        delta = newDelta;
    }

    public void setStartPos(Vector3 StartPos)
    {
        startPos = StartPos;
    }

}
