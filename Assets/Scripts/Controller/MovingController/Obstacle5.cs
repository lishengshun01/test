﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle5 : ObstacleController
{
    float delta = 5f;  // Amount to move left and right from the start point
    public float speed = 2f;
    private Vector3 startPos;

    public override void Start()
    {
        base.Start();
        startPos = new Vector3(33, transform.position.y+1, transform.position.z);
        setDelta(1.2f);
    }

    void Update()
    {
        Vector3 v = startPos;
        if(!reverted)
        v.y += delta * Mathf.Sin(Time.time * speed);
        else
            v.y -= delta * Mathf.Sin(Time.time * speed);
        transform.position = v;
    }

    public void setDelta(float newDelta)
    {
        delta = newDelta;
    }
    public void setStartPos(Vector3 StartPos)
    {
        startPos = StartPos;
    }
}
