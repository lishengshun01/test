﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle7 : Obstacle5
{
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        setStartPos(new Vector3(33, transform.position.y - 0.5f, transform.position.z));
        setDelta(1.4f);
        speed = 4;
    }

}
