﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : BehaviorController
{
    public PlayerController player;
    public Pos MovePos;
    public bool rotated = false;
    public bool reverted = false;
    float moveSpeed = 7;
    float count = 1;
    // Start is called before the first frame update
    public virtual void Start()
    {
        player = ManagerController.Instance.player.GetComponent<PlayerController>();
        //this.MovePos = CreateListPointUpDown();
        //this.StartMoving();
        //this.StartRotating360();
    }



    public void StartMoving()
    {
        moveDuration = MovePos.Point.timemove;
        __LeanTweenType = MovePos.Point.leanTweenType;
        MoveUpdateToPos(MovePos.Point.pos,Vector3.Distance(MovePos.Point.pos,transform.position)/ MovePos.Point.timemove, StartMoving);
        MovePos = MovePos.NextPoint;
    }


    public Pos CreateListPointLeftRight(int startPos,int endPos)
    {
        return PointContainer.CreateListPointWithEase(
            new Vector3(startPos, transform.position.y, transform.position.z), 
            new Vector3(endPos, transform.position.y, transform.position.z), 
            new Vector3(0.4f, 0, 0));
    }


    public void StartRotating360()
    {
        if (!rotated)
        {
            rotated = true;
            this.RotationToAngle(new Vector3(0, 0, 0), 2, StartRotating360);
        }
        else
        {
            rotated = false;
            this.RotationToAngle(new Vector3(0, 180, 0), 2, StartRotating360);
        }
    }

    public void autoRotate()
    {
        if (!reverted)
            count += 2;
        else count -= 2;
        transform.rotation = Quaternion.Euler(0, count, 0);
    }

    public void autoRotateZ()
    {
        if (!reverted)
            count += 2.3f;
        else count -= 2.3f;
        transform.rotation = Quaternion.Euler(0, 0, count);
    }

    //public void autoMovingLeftRight(int startPos, int endPos)
    //{
    //    if (!reverted)
    //        transform.localPosition -= new Vector3(startPos,transform.position.y,transform.position.z) - new Vector3(endPos, transform.position.y, transform.position.z).normalized;
    //}


    public Pos CreateListPointUpDown(float startMovePos, float endMovePos)
    {
        float offsetY = -0.4f;
        if (reverted) offsetY*= -1;
        return PointContainer.CreateListPointWithEase(
            new Vector3(transform.position.x, startMovePos, transform.position.z),
            new Vector3(transform.position.x, endMovePos, transform.position.z),
            new Vector3(0, offsetY, 0));
    }


    public Pos CreateListPointUpDownLocal(float startMovePos, float endMovePos)
    {
        float offsetY = -0.4f;
        if (reverted) offsetY *= -1;
        return PointContainer.CreateListPointWithEase(
            new Vector3(transform.position.x, startMovePos, 0),
            new Vector3(transform.position.x, endMovePos, 0),
            new Vector3(0, offsetY, 0));
    }


    public virtual void hitBehavior()
    {
        //ManagerController.Instance.player.GetComponent<PlayerController>().tryTimes--;
        //player.tryTimes--;
        //ManagerController.Instance.ui.Ammo.text = player.tryTimes;
        //player.canAttack = true;
        player.loseAmmo();
    }


}
