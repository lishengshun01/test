﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//needs more fix on player movement

public class PlayerController : BehaviorController
{
    public int tryTimes = 3;
    public int counter = 0;
    public bool canAttack = true;
    private void Start()
    {
        //canAttack = true;
        //tryTimes = 3;
        //counter = 0;
        onUpdatePlayer();
        ManagerController.Instance.player = this.gameObject;
        //animator = GetComponent<Animator>();
    }

    public void onUpdatePlayer()
    {
        canAttack = true;
        tryTimes = 3;
        counter = 0;
        transform.position = new Vector3(33, 0, 51);
    }

    private void Update()
    {

    }
    public void moveForward()
    {
        counter++;
        //if (counter == StageController.Instance.stageInfo.obstaclePosX.Length) ManagerController.Instance.OnWin();
        //animator.SetBool("isRunning", true);
        MoveToPos(new Vector3(transform.position.x, transform.position.y, transform.position.z + 15),1,stopRunAnim);
    }

    public void stopRunAnim()
    {
        canAttack = true;
        //animator.SetBool("isRunning", false);
    }

    public void loseAmmo()
    {
        tryTimes--;
        ManagerController.Instance.ui.Ammo.text = tryTimes.ToString();
        if (tryTimes <= 0)
        {
            ManagerController.Instance.OnLose();
            return;
        }
        canAttack = true;
    }
}