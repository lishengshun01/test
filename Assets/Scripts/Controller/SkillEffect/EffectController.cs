﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    PlayerController player;
    public GameObject effect;
    public Transform effectTransform;
    //Animator animator;
    void Start()
    {
        //animator = GetComponent<Animator>();
        player = GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (Input.touchCount > 0)
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (player.canAttack)
                    useEffect();
            }
        //if (Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    if (player.canAttack)
        //        useEffect();
        //}
    }

    void useEffect()
    {
        player.canAttack = false;
        //animator.SetTrigger("Attack");
        GameObject skillEffect = Instantiate(effect, effectTransform.position + transform.forward*2, Quaternion.identity);
    }
}
