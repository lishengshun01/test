﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilesController : BehaviorController
{
    public PlayerController player;
    public int speed;
    public int skillDamage;
    Vector3 direction;
    public float distance;
    public GameObject collision;
    public LayerMask collidesWith;
    Vector3 firstPos = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        player = ManagerController.Instance.player.GetComponent<PlayerController>();
        firstPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit testHit;
        if (Physics.Raycast(transform.position, transform.forward, out testHit, 1,collidesWith))
        {
            float theDistance = Vector3.Distance(firstPos, testHit.point);
            GameObject colEffect = Instantiate(collision, testHit.point - transform.forward, transform.rotation);
            if (distance > theDistance)
                distance = theDistance;

            ObstacleController target = testHit.collider.GetComponent<ObstacleController>();
            if (target != null) target.hitBehavior();
            else player.loseAmmo();


            enabled = false;
            gameObject.SetActive(false);
            Destroy(colEffect, 4);
            return;
        }
        if (Vector3.Distance(transform.position, firstPos) < distance)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
        else
        {
            player.loseAmmo();
            gameObject.SetActive(false);
        }
            
    }

}
