﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageInfo
{
    public float[] obstaclePosX;
    public float[] obstaclePosY;
    public float[] obstaclePosZ;
}

public class StageController : SingletonMonoBehavior<StageController>
{
    public StageInfo stageInfo;
    GameObject[] currentObstacles = new GameObject[3];
    int currentObstacleNumber = 0;
    // Start is called before the first frame update
    void Start()
    {
        stageInfo = DataController.stageVO.GetArrStage();
        spawnObstacles();
        //for (int i = 0; i < 1; i++)
        //{
        //}
    }

    public bool spawnObstacles()
    {
        if (currentObstacleNumber != 0)
        {
            for (int i = 0; i < currentObstacles.Length; i++)
            {
                Destroy(currentObstacles[i]);
            }
        }
        if (currentObstacleNumber > stageInfo.obstaclePosX.Length - 1) return false;
        currentObstacles[0] = CreateController.Instance.createObstacle(new Vector3(stageInfo.obstaclePosX[currentObstacleNumber], stageInfo.obstaclePosY[currentObstacleNumber], stageInfo.obstaclePosZ[currentObstacleNumber] + 15 * currentObstacleNumber), Quaternion.Euler(0, 0, 0), currentObstacleNumber);
        currentObstacles[1] = CreateController.Instance.createItem(new Vector3(Random.Range(30, 37), 1, 58 + 15 * currentObstacleNumber), Quaternion.Euler(90, 0, 0));
        currentObstacles[2] = CreateController.Instance.createDesiredItem(new Vector3(33, 1, 63 + 15 * currentObstacleNumber), Quaternion.Euler(0, 0, 0));
        currentObstacleNumber++;
        return true;
    }

    public void onReplay()
    {
        currentObstacleNumber = 0;
        for (int i = 0; i < currentObstacles.Length; i++)
        {
            Destroy(currentObstacles[i]);
        }
        spawnObstacles();
    }
}
