﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : ObstacleController
{
    public override void Start()
    {
        base.Start();
        //player = ManagerController.Instance.player;
    }
    public override void hitBehavior()
    {
        Debug.Log(transform.name);
        //ManagerController.Instance.player.GetComponent<PlayerController>().moveForward();
        //player.GetComponent<PlayerController>().moveForward();
        ManagerController.Instance.onTargetHit();
        if(StageController.Instance.spawnObstacles())
            player.moveForward(); 
        else ManagerController.Instance.OnWin();
    }
}
