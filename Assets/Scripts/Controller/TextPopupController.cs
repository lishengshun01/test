﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPopupController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //gameObject.SetActive(true);
        StartCoroutine(hideAfterDelay(0.05f));
    }
    private void OnEnable()
    {
        //StartCoroutine(hideAfterDelay());
    }

    public void startHide()
    {
        StartCoroutine(hideAfterDelay(2));
    }


    IEnumerator hideAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        //this.gameObject.SetActive(false);
        this.GetComponent<Text>().enabled = false;
    }
}
