﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text noteText;
    public Text Ammo;
    public Text OL;//Obstacle Left
    public GameObject darkScreen;
    public ButtonController replayBtn;
    // Start is called before the first frame update
    void Start()
    {
        ManagerController.Instance.ui = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void replayClicked()
    {
        ManagerController.Instance.onReplay();
    }
}
