﻿using SimpleJSON;
using UnityEngine;
public abstract class BaseVO
{
    protected JSONNode data;

    protected void ReadData(string PathData)
    {
        string txtdata = Resources.Load<TextAsset>(PathData).text;
        data = JSONNode.Parse(txtdata);
    }

}
